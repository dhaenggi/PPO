import gym
import tensorflow as tf
import numpy as np
from collections import deque
import matplotlib.pyplot as plt


class DiscretePPO(object):
    def __init__(self,
                 n_states,
                 n_actions,
                 n_neurons,
                 epsilon_clip,
                 learning_rate,
                 gamma,
                 n_update_steps,
                 session,
                 lambda_gae=0.95,
                 entropy_enabled=False,
                 global_step=None,
                 max_step=None,
                 beta=None,
                 entropy=None
                 ):
        self.n_states = n_states
        self.n_actions = n_actions
        self.n_neurons = n_neurons
        self.n_update_steps = n_update_steps

        self.session = session

        self.epsilon_clip = epsilon_clip
        self.learning_rate = learning_rate
        self.learning_rate_tensor = tf.placeholder(tf.float32, shape=[])  # for an adjustable learning rate
        self.gamma = gamma

        self.lambda_gae = lambda_gae

        self.entropy_enabled = entropy_enabled
        self.global_step = global_step
        self.max_step = max_step
        self.beta = beta
        self.entropy = entropy

        self.list_layer_names = ['ehv1', 'ehv2', 'ehp1', 'ehp2', 'pi_out_layer', 'value_layer']

        self.buffer_s = []
        self.buffer_a = []
        self.buffer_r = []
        self.buffer_v = []

        # --------- construct the graph ------------------------------------------------
        self.state = tf.placeholder(shape=[None, self.n_states], dtype=tf.float32, name='state')

        # build the policy network
        hp1 = tf.layers.dense(self.state, self.n_neurons, tf.nn.leaky_relu, trainable=True, name='ehp1')
        hp2 = tf.layers.dense(hp1, self.n_neurons, tf.nn.leaky_relu, trainable=True, name='ehp2')
        self.policy = tf.layers.dense(hp2, self.n_actions, use_bias=False, trainable=True, name='pi_out_layer')

        # probabilities for the specific actions
        self.action_probs = tf.nn.softmax(self.policy, name='action_probabilities')

        # pick the responsible action via one-hot mask
        self.action_holder = tf.placeholder(shape=[None, 1], dtype=tf.int32)
        self.selected_actions = tf.squeeze(tf.contrib.layers.one_hot_encoding(self.action_holder, self.n_actions))
        self.selected_probs = tf.multiply(self.action_probs, self.selected_actions)
        self.responsible_probs = tf.reduce_sum(self.selected_probs, axis=1)
        # prepare the previous responsible action via the same mask
        self.old_action_probs_tf = tf.placeholder(shape=[None, self.n_actions], dtype=tf.float32, name='old_probabilities')
        self.old_responsible_probs = tf.multiply(self.old_action_probs_tf, self.selected_actions)
        self.old_responsible_probs = tf.reduce_sum(self.old_responsible_probs, axis=1)

        # sampling actions
        action_output = tf.multinomial(self.action_probs, 1)
        self.action_output = tf.identity(action_output, name='action')

        # build the value estimation network
        hv1 = tf.layers.dense(self.state, self.n_neurons, tf.nn.leaky_relu, trainable=True, name='ehv1')
        hv2 = tf.layers.dense(hv1, self.n_neurons, tf.nn.leaky_relu, trainable=True, name='ehv2')
        self.value_nn = tf.layers.dense(hv2, 1, activation=None, use_bias=False, trainable=True, name='value_layer')
        self.val_reduced = tf.reduce_mean(self.value_nn, axis=1)
        self.value = tf.identity(self.value_nn, name="value_estimate")

        # prepare the loss function inputs -------------------------------------------------------------------------------
        self.returns_holder = tf.placeholder(shape=[None, 1], dtype=tf.float32, name='discounted_rewards')
        self.returns_reduced = tf.reduce_mean(self.returns_holder, axis=1)
        self.advantage_var = tf.placeholder(shape=[None, 1], dtype=tf.float32, name='advantages')
        self.advantage = tf.squeeze(self.advantage_var)
        if self.entropy_enabled:
            self.entropy = -tf.reduce_sum(self.action_probs * tf.log(self.action_probs + 1e-10), axis=1)
            self.epsilon_clip = tf.train.polynomial_decay(self.epsilon_clip, self.global_step, self.max_step, 1e-2, power=1.0)
        # add offset to old probabilities to avoid zero division
        old_prob_offset = tf.add(self.old_responsible_probs, 1e-10)
        # construct the probability ratio
        self.prob_ratio = tf.div(self.responsible_probs, old_prob_offset)  # scalar operation

        # probability ratio term for loss function
        self.ratio_term = tf.multiply(self.prob_ratio, self.advantage)
        # calculate the clipped term
        self.clipped = tf.clip_by_value(self.prob_ratio, 1 - self.epsilon_clip, 1 + self.epsilon_clip)
        self.clipped_term = tf.multiply(self.clipped, self.advantage)
        self.loss_policy = -tf.reduce_mean(tf.minimum(self.ratio_term, self.clipped_term))

        # LVFt is a squared-error loss (Vθ(st) − Vtargt)^2
        self.squared_diff = tf.squared_difference(self.returns_reduced, self.val_reduced)
        self.loss_value = tf.reduce_mean(self.squared_diff)

        # self.loss_value = tf.reduce_mean(tf.square(self.advantage_var))
        # From Schulman paper: If using a neural network architecture that shares parameters
        # between the policy and value function, we must use a loss function that combines the policy
        # surrogate and a value function error term. This objective can further be augmented by adding
        # an entropy bonus to ensure sufficient exploration, as suggested in past work [Wil92; Mni+16]
        self.loss = self.loss_policy + self.loss_value
        if self.entropy_enabled:
            decay_beta = tf.train.polynomial_decay(self.beta, self.global_step, self.max_step, 1e-5, power=1.0)
            loss_entropy = -decay_beta * tf.reduce_mean(self.entropy)
            self.loss = self.loss + loss_entropy
            self.learning_rate = tf.train.polynomial_decay(self.learning_rate, self.global_step, self.max_step, 1e-10, power=1.0)

        optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate_tensor)
        self.opt = optimizer.minimize(self.loss)

    def learn(self, s, index, identifier):
        if s.ndim < 2:
            s = s[np.newaxis, :]
        bootstrap_value = self.session.run(self.value, {self.state: s})[0][0]
        val_extended = self.buffer_v + [bootstrap_value]
        # compute delta_t = r_t + gamma * V(s+1) - V(s)
        v_t = np.vstack(val_extended[:-1])
        v_t_plus = np.vstack(val_extended[1:])
        delta_t = np.vstack(self.buffer_r) + self.gamma * v_t_plus - v_t
        # compute A_t = sum( (gamma * lambda)^(l) * delta_(t+l), l = 0 -> k-1, where k is the current element.
        adv = self.discount(r=delta_t, discount_factor=self.gamma*self.lambda_gae)
        adv = np.vstack(adv)

        # discount rewards via A = R - V -> R = A + V
        discounted_r = adv + np.vstack(self.buffer_v)

        bs = np.vstack(self.buffer_s)
        ba = np.vstack(self.buffer_a)
        br = np.vstack(discounted_r)
        self.buffer_s, self.buffer_a, self.buffer_r, self.buffer_v = [], [], [], []
        self.update(bs, ba, br, adv, identifier)
        return

    def update(self, s, a, r, adv, identifier):
        # normalize advantages and discounted rewards
        adv = (adv - adv.mean())/(adv.std()+1e-6)
        r = (r - r.mean())/(r.std()+1e-6)

        # get the old action probabilities
        self.old_action_probs = self.session.run(self.action_probs, feed_dict={self.state: s})

        feed_dict = {self.action_holder: a,
                     self.state: s,
                     self.returns_holder: r,
                     self.old_action_probs_tf: self.old_action_probs,
                     self.advantage_var: adv,
                     self.learning_rate_tensor: self.learning_rate
                     }

        [self.session.run(self.opt, feed_dict=feed_dict) for _ in range(self.n_update_steps)]
        self.update_dict = feed_dict
        return

    def store_transition(self, s, a, r, s_, index):
        self.buffer_s.append(s)
        self.buffer_a.append(a)
        self.buffer_r.append(r)
        return

    def choose_action(self, observation):
        if observation.ndim < 2:
            observation = observation[np.newaxis, :]
        fd = {self.state: observation}
        action = self.session.run(self.action_output, feed_dict=fd)[0][0]
        val = self.session.run(self.value, {self.state: observation})[0][0]
        self.buffer_v.append(val)
        return action

    def assign_logger(self, logger):
        self.logger = logger
        return

    def log_losses(self, total_step_counter, identifier):
        val_loss = self.session.run(self.loss_value, feed_dict=self.update_dict)
        policy_loss = self.session.run(self.loss_policy, feed_dict=self.update_dict)
        self.logger.log_loss(var_name='value_loss_{}'.format(identifier), var_arg=val_loss, index=total_step_counter)
        self.logger.log_loss(var_name='policy_loss_{}'.format(identifier), var_arg=policy_loss, index=total_step_counter)
        return

    def get_losses(self, total_step_counter):
        val_loss = self.session.run(self.loss_value, feed_dict=self.update_dict)
        policy_loss = self.session.run(self.loss_policy, feed_dict=self.update_dict)
        loss = self.session.run(self.loss, feed_dict=self.update_dict)
        return val_loss, policy_loss, loss

    def discount(self, r, discount_factor, value_next=0.0):
        discounted_r = np.zeros_like(r)
        running_add = value_next
        for t in reversed(range(0, r.size)):
            running_add = running_add * discount_factor + r[t]
            discounted_r[t] = running_add
        return discounted_r


if __name__ == "__main__":

    env = gym.make('CartPole-v0')  # regular reward for balancing
    states = 4
    actions = 2

    epochs = 1000
    minibatch_size = 32
    learningRate = 0.0005
    discountFactor = 0.9995

    last100Scores = [0] * 100
    last100ScoresIndex = 0
    last100Filled = False

    session = tf.Session()
    agent = DiscretePPO(n_states=states,
                        n_actions=actions,
                        n_neurons=30,
                        epsilon_clip=0.2,
                        learning_rate=learningRate,
                        gamma=discountFactor,
                        n_update_steps=5,
                        session=session,
                        entropy_enabled=False)

    session.run(tf.global_variables_initializer())

    stepCounter = 0
    loss = []
    vloss = []
    ploss = []
    scores = deque(maxlen=100)
    n_win_ticks = 195

    # number of reruns
    for epoch in range(epochs):
        observation = env.reset()
        done = False
        local_step_counter = 0

        while not done:
            # self.env.render()
            action = agent.choose_action(observation)
            newObservation, reward, done, _ = env.step(action)
            if local_step_counter > 200:
                print('well done')
                reward = 190
            agent.store_transition(observation, action, reward, newObservation, stepCounter)

            if (local_step_counter % minibatch_size == 0 or done) and local_step_counter > 0:
                agent.learn(newObservation, local_step_counter, 'Agent')
                v, p, l = agent.get_losses(local_step_counter)
                loss.append(l)
                vloss.append(v)
                ploss.append(p)

            observation = newObservation
            stepCounter += 1
            local_step_counter += 1

        if epoch % 200 == 0 and epoch > 0:
            agent.learning_rate = agent.learning_rate * 0.8

        scores.append(local_step_counter)
        mean_score = np.mean(scores)
        if mean_score >= n_win_ticks and epoch >= 100:
            print('Ran {} episodes. Solved after {} trials ✔'.format(epoch, epoch - 100))
        if epoch % 100 == 0:
            print('[Episode {}] - Mean survival time over last 100 episodes was {} ticks.'.format(epoch, mean_score))

    plt.plot(loss)
    plt.ylabel('loss')
    plt.xlabel('steps')
    plt.show()